﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using fenykepalbum_6.AspNetCore.NewDb.Models;

namespace fenykepalbum_6.Views
{
    public class Galeria : Controller
    {
        private readonly fenykepContext _context;

        public Galeria(fenykepContext context)
        {
            _context = context;
        }

        // GET: Galeria
        public async Task<IActionResult> Index( int oldalszám)
        {
            int oldalméret = 8;  //ezen az oldalon nem változtatható
            if (oldalszám==0)
            {
                oldalszám = 1;
            }
            ViewData["oldalszám"] = oldalszám;

            if (oldalszám == 1) { ViewData["előző"] = "disabled"; } else { ViewData["előző"] = ""; }
            int db = _context.Kepek.Count();
            if (oldalszám > (db / oldalméret) || (oldalszám * oldalméret == db)) { ViewData["következő"] = "disabled"; } else { ViewData["következő"] = ""; }

            return View(await _context.Kepek.Skip((oldalszám-1)*oldalméret).Take(oldalméret).ToListAsync());
        }

       
    }
}
