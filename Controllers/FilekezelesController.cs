﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using fenykepalbum_6.AspNetCore.NewDb.Models;
using fenykepalbum_6.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Fenykepalbum_6.Controllers
{
    public class Filekezeles : Controller
    {

        private readonly fenykepContext _context;
        public Filekezeles(fenykepContext context)
        {
            _context = context;
        }
        public IActionResult Index(string szűrő)
        {
            if (szűrő is null)
            {
                szűrő = "";
            }
            ViewData["szűrő"] = szűrő;
            Fileok fileok = new Fileok("wwwroot/kepek/", "feltöltésideje DESC");
            ViewData["fileok"] = fileok.fnevekidők.Where(x=>x.filenév.Contains(szűrő)).ToList();
            return View();
        }
        
        public IActionResult Töröl(string törlendő)
        {
            ViewData["adatbázisbanBejegyezve"] = _context.Kepek.Where(X => X.Filenev == törlendő).Count() > 0;         
            ViewData["törlendő"] = törlendő;
            return View();
        }
        
        public IActionResult IgenTörlés(string törlendő)
        {
            Fájl törlendőf = new Fájl(törlendő);
            
            ViewData["siker"] = törlendőf.Töröl();
           
            ViewData["törlendő"] = törlendő;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            try
            {
                List<string> márfeltöltöttek = new Fileok("wwwroot/kepek/").fnevek;
                
                if (file == null || file.Length == 0)
                    return Content("Nincs kiválasztott fájl!");

                if (márfeltöltöttek.Contains(file.FileName))
                {
                    ViewData["márvolt"] = true;
                    throw new Exception();
                }
                ViewData["filenev"] = file.FileName;
                var path = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot/kepek",
                            file.FileName);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                ViewData["siker"] = true;
            }catch { ViewData["siker"] = false; }
            return View();        
        }
       /* public async Task<IActionResult> Download(string filename)
        {
            if (filename == null)
                return Content("filename not present");

            var path = Path.Combine( Directory.GetCurrentDirectory(),"wwwroot/kepek", filename);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, ContentType(path), Path.GetFileName(path));
        }
        */
    }
}